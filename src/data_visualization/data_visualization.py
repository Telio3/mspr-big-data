import os

import pandas as pd

from src.orm.models.election import Election
from src.orm.models.participation import Participation
from src.orm.models.year import Year
from src.orm.orm import init_database_connection, close_database_connection


async def get_year_data(year: Year):
    data = {
        'year': year.id,
        'inflation': year.inflation.rate if year.inflation else None,
        'security': year.security.rate if year.security else None,
        'life_expectancy_m': year.life_expectancy.rate_m if year.life_expectancy else None,
        'life_expectancy_w': year.life_expectancy.rate_w if year.life_expectancy else None,
        'life_expectancy_average': year.life_expectancy.average if year.life_expectancy else None,
        'unemployment': year.unemployment.rate if year.unemployment else None,
        'education_rate_student': year.education.rate_student if year.education else None,
        'education_workforce': year.education.workforce if year.education else None,
        'poverty_rate_50': year.poverty.rate_50 if year.poverty else None,
        'poverty_rate_60': year.poverty.rate_60 if year.poverty else None
    }

    election = await Election.filter(year=year).order_by('-id').first()

    if not election:
        previous_election = await (Election
                                   .filter(year__id__lt=year.id)
                                   .order_by('-year__id')
                                   .first())

        if previous_election:
            participation = await (Participation
                                   .filter(election=previous_election)
                                   .first()
                                   .prefetch_related('candidate', 'position', 'voice'))

            if participation:
                data.update({
                    'winner_candidate': participation.candidate.full_name,
                    'winner_position': participation.position.name,
                    'winner_voice_total': participation.voice.total,
                    'winner_voice_rate': participation.voice.rate
                })

    elif election:
        participation = await (Participation
                               .filter(election=election)
                               .first()
                               .prefetch_related('candidate', 'position', 'voice'))

        if participation:
            data.update({
                'winner_candidate': participation.candidate.full_name,
                'winner_position': participation.position.name,
                'winner_voice_total': participation.voice.total,
                'winner_voice_rate': participation.voice.rate
            })

    return data


async def data_visualization():
    await init_database_connection()

    years = await Year.all().prefetch_related(
        'inflation',
        'security',
        'life_expectancy',
        'unemployment',
        'education',
        'poverty'
    )
    data = [await get_year_data(year) for year in years]

    await close_database_connection()

    df = pd.DataFrame(data)

    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, './data.csv')
    df.to_csv(csv_path, index=False)
