import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score
from sklearn.neural_network import MLPClassifier


def neural_network_model(x_train, y_train, x_test, y_test, labelEncoder, election):
    model = MLPClassifier(random_state=1, max_iter=3000)
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=labelEncoder.classes_)
    disp.plot()
    plt.title("Neural Network")
    plt.show()
    print("The Accuracy of NN is: ", accuracy_score(y_test, predicted) * 100)

    # Prédire pour une élection, la position gagnante
    prediction = model.predict(election)

    print("The winning position is: ", labelEncoder.inverse_transform(prediction)[0])

