import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score


def random_forest_model(x_train, y_train, x_test, y_test, labelEncoder, election):
    model = RandomForestClassifier()
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=labelEncoder.classes_)
    disp.plot()
    plt.title("Random Forest")
    plt.show()
    print("The Accuracy of RF is: ", accuracy_score(y_test, predicted) * 100)

    # Prédire pour une élection, la position gagnante
    prediction = model.predict(election)

    print("The winning position is: ", labelEncoder.inverse_transform(prediction)[0])
