import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score
from sklearn.svm import SVC


def svc_model(x_train, y_train, x_test, y_test, labelEncoder, election):
    model = SVC()
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=labelEncoder.classes_)
    disp.plot()
    plt.title("SVC")
    plt.show()
    print("The Accuracy of SVC is: ", accuracy_score(y_test, predicted) * 100)

    # Prédire pour une élection, la position gagnante
    prediction = model.predict(election)

    print("The winning position is: ", labelEncoder.inverse_transform(prediction)[0])
