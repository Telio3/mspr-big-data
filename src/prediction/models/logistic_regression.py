import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score


def logistic_regression_model(x_train, y_train, x_test, y_test, labelEncoder, election):
    model = LogisticRegression(random_state=42, max_iter=1000)
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=labelEncoder.classes_)
    disp.plot()
    plt.title("Logistic Regression")
    plt.show()
    print("The Accuracy of LR is: ", accuracy_score(y_test, predicted) * 100)

    # Prédire pour une élection, la position gagnante
    prediction = model.predict(election)

    print("The winning position is: ", labelEncoder.inverse_transform(prediction)[0])
