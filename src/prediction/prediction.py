import pandas as pd

from src.prediction.load_data import fetch_data
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
import seaborn as sns
from src.prediction.models.logistic_regression import logistic_regression_model
from src.prediction.models.neural_network import neural_network_model
from src.prediction.models.random_forest import random_forest_model
from src.prediction.models.svc import svc_model


async def prediction():
    # Charger les données
    df = await fetch_data()

    # Pré-traitement des données
    le = LabelEncoder()
    df['winning_position'] = le.fit_transform(df['winning_position'])

    # Visualiser la matrice de corrélation
    plt.figure(figsize=(16, 16))
    sns.heatmap(df.corr(), annot=True, cmap='coolwarm')
    plt.show()

    # Diviser les données en ensemble d'entraînement et ensemble de test
    X = df.drop('winning_position', axis=1)
    y = df['winning_position']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    # Prédire pour une élection, la position gagnante
    election = pd.DataFrame({
        'inflation_rate': [0.120],
        'security_rate': [0.460],
        'unemployment_rate': [0.06]
    })

    # Entraîner / Prédiction les modèles
    logistic_regression_model(X_train, y_train, X_test, y_test, le, election)
    neural_network_model(X_train, y_train, X_test, y_test, le, election)
    random_forest_model(X_train, y_train, X_test, y_test, le, election)
    svc_model(X_train, y_train, X_test, y_test, le, election)
