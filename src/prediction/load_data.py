import pandas as pd

from src.orm.models.election import Election
from src.orm.models.year import Year
from src.orm.orm import init_database_connection, close_database_connection


# async def fetch_data():
#     await init_database_connection()
#
#     elections = await Election.all().prefetch_related(
#     'participations__position',
#           'year__inflation',
#           'year__security',
#           'year__life_expectancy',
#           'year__unemployment',
#           'year__education',
#           'year__poverty'
#     ).distinct()
#
#     data = []
#
#     for election in elections:
#         row = {
#             'year': election.year.id,
#             'inflation_rate': election.year.inflation.rate if election.year.inflation else None,
#             'security_rate': election.year.security.rate if election.year.security else None,
#             'life_expectancy_average': election.year.life_expectancy.average if election.year.life_expectancy else None,
#             'unemployment_rate': election.year.unemployment.rate if election.year.unemployment else None,
#             'education_rate_student': election.year.education.rate_student if election.year.education else None,
#             'education_workforce': election.year.education.workforce if election.year.education else None,
#             'poverty_rate_50': election.year.poverty.rate_50 if election.year.poverty else None,
#             'poverty_rate_60': election.year.poverty.rate_60 if election.year.poverty else None,
#             'winning_position': election.participations[0].position.name if election.participations else None,
#         }
#
#         data.append(row)
#
#     df = pd.DataFrame(data)
#
#     print(df.head())
#
#     df.to_csv('data.csv', index=False)
#
#     await close_database_connection()


# Previous Year
async def fetch_data():
    await init_database_connection()

    elections = await Election.all().prefetch_related(
        'participations__position',
        'year'
    ).distinct()

    data = []

    for election in elections:
        previous_year = await Year.filter(id=election.year.id - 1).prefetch_related(
            'inflation',
            'security',
            'unemployment',
            'education',
            'poverty'
        ).first()

        if previous_year:
            row = {
                # 'year': election.year.id,
                'inflation_rate': previous_year.inflation.rate if previous_year.inflation else None,
                'security_rate': previous_year.security.rate if previous_year.security else None,
                'unemployment_rate': previous_year.unemployment.rate if previous_year.unemployment else None,
                # 'education_rate_student': previous_year.education.rate_student if previous_year.education else None,
                # 'education_workforce': previous_year.education.workforce if previous_year.education else None,
                # 'poverty_rate_50': previous_year.poverty.rate_50 if previous_year.poverty else None,
                # 'poverty_rate_60': previous_year.poverty.rate_60 if previous_year.poverty else None,
                'winning_position': election.participations[0].position.name if election.participations else None,
            }

            data.append(row)

    df = pd.DataFrame(data)

    await close_database_connection()

    return df
