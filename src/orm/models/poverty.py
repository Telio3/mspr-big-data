from tortoise.models import Model
from tortoise import fields


class Poverty(Model):
    id = fields.IntField(pk=True)
    rate_50 = fields.FloatField(null=True)
    rate_60 = fields.FloatField(null=True)

    def __str__(self):
        return f'50%: {self.rate_50}, 60%: {self.rate_60}'
