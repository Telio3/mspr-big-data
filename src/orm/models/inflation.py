from tortoise.models import Model
from tortoise import fields


class Inflation(Model):
    id = fields.IntField(pk=True)
    rate = fields.FloatField()

    def __str__(self):
        return self.rate
