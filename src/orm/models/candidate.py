from tortoise.models import Model
from tortoise import fields


class Candidate(Model):
    id = fields.IntField(pk=True)
    full_name = fields.CharField(max_length=255)

    def __str__(self):
        return self.full_name
