from tortoise.models import Model
from tortoise import fields


class Year(Model):
    id = fields.IntField(pk=True)
    inflation = fields.OneToOneField('models.Inflation', related_name='year', null=True)
    security = fields.OneToOneField('models.Security', related_name='year', null=True)
    life_expectancy = fields.OneToOneField('models.LifeExpectancy', related_name='year', null=True)
    unemployment = fields.OneToOneField('models.Unemployment', related_name='year', null=True)
    education = fields.OneToOneField('models.Education', related_name='year', null=True)
    poverty = fields.OneToOneField('models.Poverty', related_name='year', null=True)

    def __str__(self):
        return self.id.__str__()
