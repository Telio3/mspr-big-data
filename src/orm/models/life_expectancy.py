from tortoise.models import Model
from tortoise import fields


class LifeExpectancy(Model):
    id = fields.IntField(pk=True)
    rate_m = fields.FloatField()
    rate_w = fields.FloatField()
    average = fields.FloatField()

    def __str__(self):
        return self.average
