from tortoise.models import Model
from tortoise import fields


class Unemployment(Model):
    id = fields.IntField(pk=True)
    rate = fields.FloatField()

    def __str__(self):
        return self.rate
