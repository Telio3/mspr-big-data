from tortoise.models import Model
from tortoise import fields


class Education(Model):
    id = fields.IntField(pk=True)
    rate_student = fields.FloatField()
    workforce = fields.FloatField()

    def __str__(self):
        return self.rate_student
