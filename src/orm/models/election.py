from tortoise.models import Model
from tortoise import fields


class Election(Model):
    id = fields.IntField(pk=True)
    year = fields.ForeignKeyField('models.Year', related_name='elections')
    # city = fields.ForeignKeyField('models.City', related_name='elections')

    def __str__(self):
        return f"{self.year} Election"
