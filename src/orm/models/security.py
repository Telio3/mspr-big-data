from tortoise.models import Model
from tortoise import fields


class Security(Model):
    id = fields.IntField(pk=True)
    rate = fields.FloatField()

    def __str__(self):
        return self.rate
