from tortoise.models import Model
from tortoise import fields


class Voice(Model):
    id = fields.IntField(pk=True)
    total = fields.IntField()
    rate = fields.FloatField()

    def __str__(self):
        return f"{self.total} voices - {self.rate} %"
