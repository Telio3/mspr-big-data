from tortoise.models import Model
from tortoise import fields


class Participation(Model):
    id = fields.IntField(pk=True)
    voice = fields.OneToOneField('models.Voice', related_name='participation')
    candidate = fields.ForeignKeyField('models.Candidate', related_name='participations')
    position = fields.ForeignKeyField('models.Position', related_name='participations')
    election = fields.ForeignKeyField('models.Election', related_name='participations')

    def __str__(self):
        return f"{self.candidate} for {self.position} in {self.election}"
