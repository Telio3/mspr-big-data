from src.orm.models.candidate import Candidate
from src.orm.models.city import City
from src.orm.models.education import Education
from src.orm.models.election import Election
from src.orm.models.inflation import Inflation
from src.orm.models.life_expectancy import LifeExpectancy
from src.orm.models.participation import Participation
from src.orm.models.position import Position
from src.orm.models.poverty import Poverty
from src.orm.models.security import Security
from src.orm.models.unemployment import Unemployment
from src.orm.models.voice import Voice
from src.orm.models.year import Year


async def reset():
    await Year.all().delete()
    await Inflation.all().delete()
    await Candidate.all().delete()
    await Education.all().delete()
    await Election.all().delete()
    await LifeExpectancy.all().delete()
    await Participation.all().delete()
    await Position.all().delete()
    await Poverty.all().delete()
    await Security.all().delete()
    await Unemployment.all().delete()
    await Voice.all().delete()
    await City.all().delete()
