import os

import pandas as pd

from src.orm.models.inflation import Inflation
from src.orm.models.year import Year


async def inflation_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, '../../../datasets/inflation_out.csv')
    df = pd.read_csv(csv_path, sep=';')

    for index, row in df.iterrows():
        year = await Year.get(id=row['Annee'])

        rate = row['Inflation_Moyenne_Annuelle'] / 100
        rate = round(rate, 4)

        inflation = await Inflation.create(rate=rate)

        year.inflation = inflation

        await year.save()
