import os

import pandas as pd

from src.orm.models.education import Education
from src.orm.models.year import Year


async def education_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, '../../../datasets/education.csv')
    df = pd.read_csv(csv_path, sep=';')

    for index, row in df.iterrows():
        year = await Year.get(id=row['Annee'])

        average_student_expense = row['Depense_moyenne_par_étudiant']
        average_student_expense -= 100
        average_student_expense /= 100
        average_student_expense = round(average_student_expense, 4)

        workforce = row['Effectifs_de_l_enseignement_supérieur']
        workforce -= 100
        workforce /= 100
        workforce = round(workforce, 4)

        education = await Education.create(
            rate_student=average_student_expense,
            workforce=workforce
        )

        year.education = education

        await year.save()
