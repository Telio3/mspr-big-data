from src.orm.data_filling.education import education_filling
from src.orm.data_filling.inflation import inflation_filling
from src.orm.data_filling.life_expectancy import life_expectancy_filling
from src.orm.data_filling.participation import participation_filling
from src.orm.data_filling.position import position_filling
from src.orm.data_filling.poverty import poverty_filling
from src.orm.data_filling.security import security_filling
from src.orm.data_filling.unemployment import unemployment_filling
from src.orm.data_filling.year import year_filling
from src.orm.orm import init_database_connection, close_database_connection, generate_schemas
from src.orm.reset import reset


async def data_filling():
    await init_database_connection()
    await generate_schemas()
    await reset()

    await year_filling()
    await position_filling()
    await inflation_filling()
    await security_filling()
    await life_expectancy_filling()
    await unemployment_filling()
    await education_filling()
    await poverty_filling()
    await participation_filling()

    await close_database_connection()
