import os

import pandas as pd

from src.orm.models.unemployment import Unemployment
from src.orm.models.year import Year


async def unemployment_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, '../../../datasets/chomage_out.csv')
    df = pd.read_csv(csv_path, sep=';')

    for index, row in df.iterrows():
        year = await Year.get(id=row['Libelle'])

        rate = row['Taux'] / 100
        rate = round(rate, 4)

        unemployment = await Unemployment.create(rate=rate)

        year.unemployment = unemployment

        await year.save()
