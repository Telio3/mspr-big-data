import os

import pandas as pd

from src.orm.models.security import Security
from src.orm.models.year import Year


async def security_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, '../../../datasets/tx_criminalite.csv')
    df = pd.read_csv(csv_path, sep=';')

    for index, row in df.iterrows():
        year = await Year.get(id=row['Annee'])

        rate = row['Taux_de_criminalite'] / 100
        rate = round(rate, 4)

        security = await Security.create(rate=rate)

        year.security = security

        await year.save()
