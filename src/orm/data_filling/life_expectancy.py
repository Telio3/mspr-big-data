import os

import pandas as pd

from src.orm.models.life_expectancy import LifeExpectancy
from src.orm.models.year import Year


async def life_expectancy_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, '../../../datasets/esperance_vie_out.csv')
    df = pd.read_csv(csv_path, sep=';')

    for index, row in df.iterrows():
        year = await Year.get(id=row['Annees'])

        life_expectancy = await LifeExpectancy.create(
            rate_m=row['Hommes'],
            rate_w=row['Femmes'],
            average=row['Moyennes']
        )

        year.life_expectancy = life_expectancy

        await year.save()
