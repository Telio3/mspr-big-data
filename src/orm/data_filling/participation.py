import os

import pandas as pd

from src.orm.models.candidate import Candidate
from src.orm.models.city import City
from src.orm.models.election import Election
from src.orm.models.participation import Participation
from src.orm.models.position import Position
from src.orm.models.voice import Voice
from src.orm.models.year import Year


async def participation_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))

    years = [1965, 1969, 1974, 1981, 1988, 1995, 2002, 2007, 2012, 2017, 2022]
    file_name = "_2_results.csv"

    positions_csv_path = os.path.join(current_dir, '../../../datasets/candidat_position.csv')
    df_positions = pd.read_csv(positions_csv_path, sep=';')

    for y in years:
        csv_path = os.path.join(current_dir, f'../../../datasets/resultats_presidentiels/{y}{file_name}')
        df = pd.read_csv(csv_path, sep=';')

        year = await Year.get(id=y)

        election = await Election.create(year=year)

        for index, row in df.iterrows():
            voice_str = row['exprimes']
            voice_float = float(voice_str.strip('%')) / 100
            voice_formatted = round(voice_float, 4)

            total = int(row['voix'].replace(' ', ''))

            voice = await Voice.create(
                total=total,
                rate=voice_formatted
            )

            candidate = await Candidate.create(full_name=row['candidat'])

            p = df_positions.loc[df_positions['candidat'] == row['candidat'], 'position'].values[0]

            position = await Position.get(name=p)

            await Participation.create(
                election=election,
                candidate=candidate,
                voice=voice,
                position=position
            )

# async def participation_filling():
#     current_dir = os.path.dirname(os.path.abspath(__file__))
#
#     csv_path = os.path.join(current_dir, '../../../datasets/resultat_election_municipale.csv')
#     df = pd.read_csv(csv_path, sep=',')
#
#     for index, row in df.iterrows():
#         year = await Year.get(id=row['annee'])
#         city = await City.get_or_create(name=row['ville'])
#         election = await Election.create(
#             year=year,
#             city=city[0],
#         )
#         candidate = await Candidate.create(full_name=row['maire'])
#         position = await Position.get(name=row['position'])
#
#         await Participation.create(
#             election=election,
#             candidate=candidate,
#             position=position,
#         )
