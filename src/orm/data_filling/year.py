from src.orm.models.year import Year


async def year_filling():
    for year in range(1900, 2025):
        year_instance = Year(id=year)
        await year_instance.save()
