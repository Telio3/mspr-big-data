import os

import pandas as pd

from src.orm.models.poverty import Poverty
from src.orm.models.year import Year


async def poverty_filling():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    csv_path = os.path.join(current_dir, '../../../datasets/pauvrete.csv')
    df = pd.read_csv(csv_path, sep=';')

    for index, row in df.iterrows():
        year = await Year.get(id=row['Annee'])

        rate_50 = row['Seuil_de_50%_HR']
        rate_60 = row['Seuil_de_60%_HR']

        if pd.isna(rate_50):
            rate_50 = None
        else:
            rate_50 /= 100
            rate_50 = round(rate_50, 4)

        if pd.isna(rate_60):
            rate_60 = None
        else:
            rate_60 /= 100
            rate_60 = round(rate_60, 4)

        poverty = await Poverty.create(
            rate_50=rate_50,
            rate_60=rate_60
        )

        year.poverty = poverty

        await year.save()
