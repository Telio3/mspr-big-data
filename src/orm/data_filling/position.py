from src.orm.models.position import Position


async def position_filling():
    for position in ['gauche', 'droite']:
        await Position.create(name=position)
