from tortoise import Tortoise


async def init_database_connection():
    await Tortoise.init(
        db_url='mysql://myuser:mypass@localhost:3306/mydb',
        modules={
            'models': [
                'src.orm.models.year',
                'src.orm.models.inflation',
                'src.orm.models.security',
                'src.orm.models.life_expectancy',
                'src.orm.models.unemployment',
                'src.orm.models.education',
                'src.orm.models.poverty',
                'src.orm.models.candidate',
                'src.orm.models.election',
                'src.orm.models.participation',
                'src.orm.models.position',
                'src.orm.models.voice',
                'src.orm.models.city'
            ]
        }
    )


async def generate_schemas():
    await Tortoise.generate_schemas()


async def close_database_connection():
    await Tortoise.close_connections()
