from tortoise import run_async


def main():
    while True:
        print("-" * 50)

        user_input = input("Select an option:\n0 - Exit\n1 - Data Filling\n2 - Prediction\n3 - Data "
                           "Visualization\nEnter the number corresponding to your choice: ")

        if user_input.isdigit():
            user_choice = int(user_input)

            if user_choice == 0:
                print("Exiting the program.")
                break
            elif user_choice == 1:
                from src.orm.data_filling.data_filling import data_filling
                run_async(data_filling())
                print("Data filling completed.")
            elif user_choice == 2:
                from src.prediction.prediction import prediction
                run_async(prediction())
                print("Prediction completed.")
            elif user_choice == 3:
                from src.data_visualization.data_visualization import data_visualization
                run_async(data_visualization())
                print("Data visualization completed.")
            else:
                print("Invalid input. Please enter 1 or 2.")
        else:
            print("Invalid input. Please enter a valid number.")


if __name__ == "__main__":
    main()
